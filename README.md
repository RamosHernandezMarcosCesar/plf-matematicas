## Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .salmon {
    BackgroundColor LightSalmon
  }
  .lime {
    BackgroundColor Lime
  }
  .orchid {
    BackgroundColor Orchid
  }
  .crimson {
    BackgroundColor Crimson
    Fontcolor white
  }
  .gold {
    BackgroundColor Gold
  }
  .orange {
    BackgroundColor Orange
  }
  .cyan {
    BackgroundColor lightcyan
  }
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
}
</style>
* Conjunto, aplicaciones y funciones <<salmon>>
 * conjunto <<orchid>>
  * no se define
   *_ se da
    * por intuitiva
  *_ es
   * una idea intuitiva asumida\npor el observador <<orange>>
  * conjunto y elemento <<gold>>
   *_ son
    * conceptos primitivos que no\nse definen <<blue>>
   *_ la
    * relación entre ellos se llama <<blue>>
     * relación de pertenencia <<cyan>>
      *_ que es
       * dado un elemento y un conjunto\nse puede saber si el elemento esta\no no esta en el conjunto <<cyan>>
   *_ forman la
    * teoría de conjuntos <<blue>>
  * inclusión de conjuntos <<gold>>
   *_ es
    * la primera noción que hay\nentre dos conjuntos
   * un conjunto esta contenido en\notro si todo elemento del primero\npertenece al otro
  * operaciones de conjuntos <<gold>>
   *_ son
    * conjuntos mas complejos armados
     *_ sobre 
      * los conjuntos elementales <<cyan>>
   * operaciones básicas
    * intersección de conjuntos <<lime>>
     *_ son
      * elementos que pertenecen simultaneamente\na ambos conjuntos <<cyan>>
    * unión de conjuntos <<lime>>
     *_ son
      * elementos que pertenecen a uno\nde ambos conjuntos <<cyan>>
    * complementación de conjuntos <<lime>>
     *_ son
      * elementos que no pertenecen a\nun conjunto dado <<cyan>>
    * diferencia de conjuntos <<lime>>
  * tipos <<gold>>
   * universal <<lime>>
    *_ es
     * un conjunto de referencia
      *_ donde
       * ocurren todas las cosas de\nla teoría <<cyan>>
   * vacío <<lime>>
    *_ es
     * conjunto que no tiene elementos
      *_ surge
       * por una necesidad lógica\npara cerrar bien <<cyan>>
  * diagrama de venn <<gold>>
   *_ es
    * una representación de los elementos\nde los conjuntos <<rose>>
  * cardinal de un conjunto <<gold>>
   *_ es
    * el número de elementos que\nconforman el conjunto <<rose>>
     *_ es
      * un número natural
   *_ tiene
    * propiedades <<rose>>
     * fórmula
     * acotación de cardinales
 * aplicación <<orchid>>
  *_ es 
   * una transformación
    *_ que
     * convierte a cada uno de los elementos\nde un conjunto inicial <<rose>>
      *_ en
       * un unico elemento de un\nconjunto destino
  *_ se da
   * en cualquier disciplina cientifica\nque estudia las transformaciones <<rose>>
  *_ tiene
   * distintos tipos <<blue>>
    *_ como
     * inyectiva <<lime>>
     * subyectiva <<lime>>
     * biyectiva <<lime>>
   * ideas abstractas <<blue>>
    *_ como 
     * imágen e imagen inversa <<lime>>
     * composición de aplicaciones <<lime>>
     * transformación de transformacion <<lime>>
 * función <<orchid>>
  *_ son
   * aplicaciones mas faciles de ver <<blue>>
    *_ y 
     * pueden ser representados <<rose>>
      *_ de
       * manera gráfica
  *_ ejemplo
   * conjunto de números en\nun eje de coordenadas <<blue>>
    *_ representan
     * una serie de puntos <<rose>>
      *_ forman
       * una figura
@endmindmap
```
## Funciones (2010)
```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    :depth(1) {
      BackGroundColor lightpink
    }
    :depth(2) {
      BackGroundColor lightcyan
    }
    :depth(3) {
      BackGroundColor gold
    }
    :depth(4) {
      BackGroundColor orchid
    }
    :depth(5) {
      BackGroundColor lightsalmon
    }
    :depth(6) {
      BackGroundColor business
    }
    :depth(7) {
      BackGroundColor aquamarine
    }
}
</style>
* Funciones
 *_ refleja
  * lo que el pensamiento del\nhombre a hecho
   *_ para
    * entender su entorno
     *_ y
      * junto con las matemáticas\nresolver problemas
 * idea de función
  *_ el hombre
   * se enfrenta al cambio
  *_ el afán
   * de hombre es decubrir\ncomo las matemáticas
    *_ ayudan
     * a entender el cambio
 * la función
  *_ es 
   * una regla que pasa un conjunto\nde números a otro conjunto\nde números
    *_ en especial
     * los números reales
 * representación
  *_ se utiliza
   * el plano cartesiano
    *_ para
     * representar los conjuntos\nde los números
      *_ donde se tiene
       * un número x y su imagen f(x)\nrepresentado como y
    * la grafica
     *_ da
      * una idea de como cambia el\nconjunto primero al conjunto segundo
 * el cuestionamiento y la\nexperimentacion del hombre
  *_ llevan a
   * funciones crecientes
    *_ significa que
     * cuando aumenta la variable\nindependiente
      *_ tambien
       * aumenta sus imagenes
   * funciones decrecientes
    *_ significa que
     * cuando aumenta la variable\nindependiente
      *_ las
       * imagenes disminuyen
 * comportamiento de las funciones
  * intervalo de una funcíon
   *_ es
    * el rango de valores que puede\ntomar una variable
  * máximo de una función
   *_ es
    * el valor más alto en x que\npuede llegar a tener una función
  * mínimo de una función
   *_ es
    * el valor mas bajo en x que\npuede llegar a tener una función
  * límite de una función
   *_ en un 
    * punto
     *_ siginica que
      * los valores de la variable y\nestan cerca de un valor
  * idea de límite
   *_ son 
    * los valores de aproximación
     *_ cuando
      * nos acercamos al punto de interés
  * continuidad de una función
   *_ es
    * una función que tiene\nbuen comportamiento
     *_ sin
      * discontinuidad ni saltos
 * cálculo diferencial
  * derivada
   *_ la idea surge de
    * como aproximar una función\nrelativamente complicada
     *_ con
      * una función mas simple
   * resuelve
    * el problema de la aproximación\nde una función compleja
     *_ mediante
      * una función lineal
@endmindmap
```
## La matemática del computador (2002)
```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor tomato
    }
    :depth(1) {
      BackGroundColor lightcyan
    }
    :depth(2) {
      BackGroundColor lightgreen
    }
    :depth(3) {
      BackGroundColor plum
    }
    :depth(4) {
      BackGroundColor business
    }
    :depth(5) {
      BackGroundColor white
    }
    :depth(6) {
      BackGroundColor lightpink
    }
    :depth(7) {
      BackGroundColor aquamarine
    }
    :depth(8) {
      BackGroundColor yellowgreen
    }
    :depth(9) {
      BackGroundColor yellowgreen
    }
}
</style>
* Matemática del computador
 * aritmética del computador
  * representación de los números
   *_ los números
    * con infinitas racionales\ncon infinitas cifras
     *_ no se pueden
      * representar en un computador
  * las matemáticas
   *_ ayudan
    * con el problema de la representacion\nde los decimales infinitos
     *_ representandolos
      * en un número finito\nde posiciones 
  *_ para representar
   * los números demasiado grandes
    *_ se tiene que
     * acortar o redondear
    *_ y
     * demasiado pequeños
      *_ se usa
       * el producto de un número\npor una potencia de 10
        *_ esto permite
         * dar una magnitud\ndel número
         * conservar un orden
  * acortamiento
   *_ es
    * despreciar un número\nde cifras
  * redondeo
   *_ es
    * un truncamiento refinado
    * despreciar un numero\nde cifras 
     *_ y
      * retocar la ultima cifra
       *_ para
        * tener el menor\nerror posible
 * sistema binario
  *_ es
   * el sistema en el que\nse mueven los computadores
    *_ mediante 
     * la ausencia y presencia\nde corriente
  *_ con este sistema
   * se pueden armar\nestructras complejas
 * codificación
  *_ es la
   * capacidad para representar\nen un computador
    *_ cosas como
     * operaciones aritméticas
      * en sistema binario
     * letras
      * en sistema binario
    *_ la representación
     * de un número en un\nnúmero finito de posiciones
      *_ es
       * mediante bits
 * sistemas octal y\nhexadecimal
  *_ se usan para
   * representar números demasiado\ngrandes dentro de un\ncomputador
@endmindmap
```